#!/usr/bin/env python
#description     :Rolldown simulator - generates simulated rolldown data
#author          :varvara
#date            :20140610
#==============================================================================

import numpy as np
from scipy import integrate
import matplotlib.pyplot as plt
from pylab import *

# === simulation settings ===
Crr = 0.01
CdA = 0.18
V = [27] # array of initial velocities to simulate
T = 80.0             # number of seconds to simulate for
ERR = 0.1 # m/s

# === output ===
CSVOUT = "simrd"     # filename prefix for writing sim output data
                     # (outputs in ./canlog_data_extractor csv format)

# === constants ===
G = -9.81
M = 380.0
RHO = 1.226

def main():
    # Initialise plot
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    ax1.set_title("Rolldown")
    ax1.set_xlabel("t (s)")
    ax1.set_ylabel("v (ms-1)")
    ax1.set_xlim(xmin=0, xmax=T)
    ax1.set_ylim(ymin=0, ymax=max(V))

    for vinit in V:
        # integrate ODE for this vinit
        t, v = run(vinit, T)
        ax1.plot(t, v)

        # add noise to velocities
        noise = np.random.normal(0,ERR,len(v))
        vnoise = np.add(v, noise)
        ax1.scatter(t, vnoise)

        # write to csv in STEVE_SPEED output format
        vmph = vnoise*3600 # want speed in m/h
        out = np.column_stack((t, vmph))
        np.savetxt(CSVOUT+str(vinit)+".csv", out, delimiter=",", fmt='%.8f')

    plt.show()

def run(vinit, tend):
    # integrate ODE
    time = linspace(0.0, tend, tend)
    vms = integrate.odeint(dvdt, vinit, time).flatten()
    return time, vms

def dvdt(v, t):
    # ODE definition
    return Crr*G - 0.5*RHO*CdA*(v**2)/M

if __name__ == "__main__":
        main()
