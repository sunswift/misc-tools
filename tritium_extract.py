import sys, os

"""
Extracts data from tritium log files in .csv format.
Accepts input filename, output filename, message id desired and the field number
you wish to extract (field number is simply 1st or 2nd field). 

EG if you wanted to extract bus current measurement, message id would be 2
and field number would be 1. See http://tritium.com.au/wp-content/uploads/2013/07/TRI88.004v3_Users_Manual.pdf
to find message IDs and field numbers (starts page 32).

Output is a csv file with packet number, marand data, csiro data where the data is 
whatever field you selected. In practice each line will only have either csiro or marand,
but most graphing tools should ignore this allowing easy graphical comparison.

Packet number is used as multiple packets may occur per time unit.
"""

# Device IDs for the marand and csiro motors
marand_id = 0x400 >> 5
csiro_id = 0x420 >> 5

if __name__ == "__main__":
    # User input handling
    if len(sys.argv) == 5:
        filename_in = sys.argv[1]
        filename_out = sys.argv[2]
        message_id = int(sys.argv[3])
        field_num = int(sys.argv[4])

    elif len(sys.argv) != 1:
        print("Usage: [filename in] [filename out] [Message ID] [Field number (1 or 2)")
        print("Alternatively use the interactive input by running with no arguments")
        sys.exit(2)

    else:
        filename_in = input("Input filename: ")
        filename_out = input("Output filename: ")
        message_id = int(input("Enter which message num: "))
        field_num = int(input("Enter which field to return: "))

    if not os.path.exists(filename_in):
        sys.exit("Input file does not exist")

    fin = open(filename_in, "r")
    fout = open(filename_out, "w")
    fin.readline()
    for line in fin:
        # Get packet information
        # See http://tritium.com.au/wp-content/uploads/2013/07/TRI88.004v3_Users_Manual.pdf
        packet = line.split(",")
        if packet[3].strip() != "":
            control = int(packet[3], 16)
        else:
            control = 0
        identifier = int(packet[2], 16)
        packet_id = int(packet[1])
        time = packet[0]

        # Get identifiers
        message_identifier = identifier & 0x1F
        identifier = identifier >> 5
        device_identifier = identifier & 0x3F

        # Get float data from field
        if field_num == 1:
            data = packet[5]
        else:
            data = packet[6]
        data = data.strip()

        # Push output
        if message_identifier == message_id:
            if device_identifier == marand_id:
                fout.write(""+str(packet_id)+","+data+",\n")
            elif device_identifier == csiro_id:
                fout.write(""+str(packet_id)+",,"+data+"\n")

    fin.close()
    fout.close()
